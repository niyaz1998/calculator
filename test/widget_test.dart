// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:math';

import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/expression.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/factor.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/number.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/power_of.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/sqrt.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/term.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/expressions/variable.dart';
import 'package:expression_caculator/repositories/calculators/custom_calculator/parser.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {
  double solve(String expression, [double value = 0]) {
    Parser parser = Parser(expression);
    Expression ast = parser.parse();
    return ast.calculate(value);
  }

  test('basic expressions tests', () {
    double v1 = 4;
    double v2 = 8;
    NumberExpression var1 = NumberExpression(v1);
    NumberExpression var2 = NumberExpression(v2);

    expect(var1.calculate(0), 4);
    expect(Factor(FactorOperator.DIV, var2, var1).calculate(0), v2 / v1);
    expect(Factor(FactorOperator.MULT, var2, var1).calculate(0), v1 * v2);
    expect(Factor(FactorOperator.NONE, var2, var1).calculate(0), v2);

    expect(
        PowerOf(PowerOfOperators.POWER, var2, var1).calculate(0), pow(v2, v1));
    expect(PowerOf(PowerOfOperators.NONE, var2, var1).calculate(0), v2);

    expect(SqrtExpression(var1).calculate(0), sqrt(v1));

    expect(Term(TermOperator.MINUS, var2, var1).calculate(0), v2 - v1);
    expect(Term(TermOperator.PLUS, var2, var1).calculate(0), v2 + v1);
    expect(Term(TermOperator.NONE, var2, var1).calculate(0), v2);

    expect(Variable().calculate(v1), v1);
    expect(Variable().calculate(v2), v2);
  });

  test('basic terms test', () {
    expect(solve('2+2'), 4);
    expect(solve('2+2+2'), 6);
    expect(solve('2-2'), 0);
    expect(solve('2-2+2'), 2);
    expect(solve('2-2-2'), -2);


    expect(solve('2+x', 1), 3);
    expect(solve('2+x', 2), 4);
  });

  test('basic factors test', () {
    expect(solve('2*2'), 4);
    expect(solve('2*2*2'), 8);
    expect(solve('2/2'), 1);
    expect(solve('2/2*2'), 2);
    expect(solve('2*2/2'), 2);


    expect(solve('2*x', 1), 2);
    expect(solve('2/x', 2), 1);
  });

  test('basic power of test', () {
    expect(solve('2^2'), 4);
    expect(solve('2^2^2'), 16);


    expect(solve('2^x', 2), 4);
    expect(solve('2^x', 3), 8);
  });

  test('basic parenthesized test', () {
    expect(solve('(2+2)*2'), 8);
    expect(solve('2*(2+2)*2'), 16);
    expect(solve('(2+2)*(4-2)'), 8);
  });

  test('complex expressions test', () {
    expect(solve('2 + 4 - 8 / (4 - 2) + sqrt(9) * (sqrt(4^2) / 2)'), 8);
    expect(solve('(2*(2+2) / (sqrt(4) / 2 + 1))'), 4);
  });
}
