import 'package:json_annotation/json_annotation.dart';

part 'query_result.g.dart';

/// classes used to parse response from wolfram alpha
/// they doesn't parse all result, just part in which application is interested in

@JsonSerializable(nullable: false)
class WolframAlphaResponse {
  QueryResult queryresult;

  WolframAlphaResponse({this.queryresult});

  factory WolframAlphaResponse.fromJson(Map<String, dynamic> json) =>
      _$WolframAlphaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$WolframAlphaResponseToJson(this);
}

@JsonSerializable(nullable: false)
class QueryResult {
  bool error;
  List<Pod> pods;

  QueryResult({this.error, this.pods});

  factory QueryResult.fromJson(Map<String, dynamic> json) =>
      _$QueryResultFromJson(json);

  Map<String, dynamic> toJson() => _$QueryResultToJson(this);
}

@JsonSerializable(nullable: false)
class Pod {
  String title;
  String id;
  bool error;
  List<SubPod> subpods;

  Pod({this.title, this.id, this.error, this.subpods});

  factory Pod.fromJson(Map<String, dynamic> json) => _$PodFromJson(json);

  Map<String, dynamic> toJson() => _$PodToJson(this);
}

@JsonSerializable(nullable: false)
class SubPod {
  String title;
  String plaintext;

  SubPod({this.title, this.plaintext});

  factory SubPod.fromJson(Map<String, dynamic> json) => _$SubPodFromJson(json);

  Map<String, dynamic> toJson() => _$SubPodToJson(this);
}
