// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'query_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WolframAlphaResponse _$WolframAlphaResponseFromJson(Map<String, dynamic> json) {
  return WolframAlphaResponse(
      queryresult:
          QueryResult.fromJson(json['queryresult'] as Map<String, dynamic>));
}

Map<String, dynamic> _$WolframAlphaResponseToJson(
        WolframAlphaResponse instance) =>
    <String, dynamic>{'queryresult': instance.queryresult};

QueryResult _$QueryResultFromJson(Map<String, dynamic> json) {
  return QueryResult(
      error: json['error'] as bool,
      pods: (json['pods'] as List)
          .map((e) => Pod.fromJson(e as Map<String, dynamic>))
          .toList());
}

Map<String, dynamic> _$QueryResultToJson(QueryResult instance) =>
    <String, dynamic>{'error': instance.error, 'pods': instance.pods};

Pod _$PodFromJson(Map<String, dynamic> json) {
  return Pod(
      title: json['title'] as String,
      id: json['id'] as String,
      error: json['error'] as bool,
      subpods: (json['subpods'] as List)
          .map((e) => SubPod.fromJson(e as Map<String, dynamic>))
          .toList());
}

Map<String, dynamic> _$PodToJson(Pod instance) => <String, dynamic>{
      'title': instance.title,
      'id': instance.id,
      'error': instance.error,
      'subpods': instance.subpods
    };

SubPod _$SubPodFromJson(Map<String, dynamic> json) {
  return SubPod(
      title: json['title'] as String, plaintext: json['plaintext'] as String);
}

Map<String, dynamic> _$SubPodToJson(SubPod instance) =>
    <String, dynamic>{'title': instance.title, 'plaintext': instance.plaintext};
