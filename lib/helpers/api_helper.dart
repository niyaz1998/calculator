import 'dart:convert';

import 'package:expression_caculator/model/wolfram_alpha/query_result.dart';
import 'package:http/http.dart' as http;

import 'internet.dart';

///-------------------------------------------
///flutter packages pub run build_runner build
///-------------------------------------------

/// Class that makes an api calls
class ApiHelper {
  static const APP_ID = 'P65VUH-5G94XE8P9L';

  static Future<QueryResult> getPoints(String expression, double startPoint,
      double endPoint, double stepSize) async {
    var response = await WolframAlphaHelper.get('/query', parameters: {
      'appid': APP_ID,
      'input': Uri.encodeComponent('TABLE[$expression,{x,$startPoint,$endPoint,$stepSize})]'),
      'output': 'json',
      'format': 'plaintext',
    });
    _checkOutput(response, 'solving expression wolfram alpha');
    return WolframAlphaResponse.fromJson(json.decode(response.body))
        .queryresult;
  }

  static void _checkOutput(http.Response response, [String action]) {
    print("action: $action, code: ${response.statusCode}. Response: ");
    print(response.body);
    if (response.statusCode < 200 || response.statusCode >= 300) {
      throw ServerError(response.statusCode);
    }
  }
}

class ServerError {
  int statusCode;

  ServerError(this.statusCode);

  @override
  String toString() {
    return 'code $statusCode';
  }
}
