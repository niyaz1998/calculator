import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

/// makes http requests to wolfram alpha
class WolframAlphaHelper {
  static const String url = 'http://api.wolframalpha.com/v2/';

  static const Map<String, String> defaultHeaders = const {
    'Content-Type': 'application/json'
  };

  static Future<http.Response> get(String path,
      {Map<String, dynamic> parameters, Map<String, String> headers}) async {
    print(Uri.encodeFull(url + _addParamsToUrl(parameters, path)));
    return await http.get(
      url + _addParamsToUrl(parameters, path),
      headers: headers,
    );
  }

  static Future<http.Response> post(String path,
      {Map<String, dynamic> parameters,
      String jsonString,
      Map<String, String> headers}) async {
    return await http.post(
      url + _addParamsToUrl(parameters, path),
      headers: headers,
      body: jsonString,
    );
  }

  static Future<http.Response> put(String path,
      {Map<String, dynamic> parameters,
      String jsonString,
      Map<String, String> headers}) async {
    return await http.put(
      url + _addParamsToUrl(parameters, path),
      headers: headers,
      body: jsonString,
    );
  }

  /// forms a string with parameters like '/some/link?key1=value1&key2=value2'
  static String _addParamsToUrl(Map<String, dynamic> parameters, String path) {
    if (parameters != null) {
      path += '?';
      for (String key in parameters.keys) {
        path += key + '=' + parameters[key].toString() + '&';
      }
      path = path.substring(0, path.length - 1); // remove last '&'
    }
    return path;
  }
}
