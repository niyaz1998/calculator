import 'dart:math';

import '../calculator.dart';
import 'expressions/expression.dart';
import 'parser.dart';

/// this calculator uses [Parser] to build expressions tree and build list of points
class CustomCalculatorRepository extends AbstractCalculatorRepository {
  @override
  String get name => 'Custom';

  @override
  Future<List<Point<double>>> getChart(
    String expression,
    double startPoint,
    double endPoint, [
    int pointsAmount = 100,
  ]) async {
    // swap values if starting point is after last point
    if (startPoint > endPoint) {
      double temp = endPoint;
      endPoint = startPoint;
      startPoint = temp;
    }

    double stepSize = (endPoint - startPoint) / pointsAmount;

    List<Point<double>> result = List();
    Parser parser = Parser(expression);
    Expression parsedExpression = parser.parse();

    double i = startPoint;
    while (i < endPoint) {
      result.add(Point(i, parsedExpression.calculate(i)));
      i = i + stepSize;
    }
    result.add(Point(endPoint, parsedExpression.calculate(endPoint)));

    await Future.delayed(Duration(seconds: 1));
    return result;
  }
}
