import 'dart:math';

import 'expression.dart';
import 'primary.dart';

/// represents expression in "sqrt()"
class SqrtExpression extends Primary {
  Expression expression;

  SqrtExpression(this.expression);

  @override
  double calculate(double variableValue) {
    return sqrt(expression.calculate(variableValue));
  }
}
