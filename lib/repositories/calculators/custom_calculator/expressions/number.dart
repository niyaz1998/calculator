import 'primary.dart';

/// wrapper over numerical value
class NumberExpression extends Primary {
  double value;

  NumberExpression(this.value);

  @override
  double calculate(double variableValue) {
    return value;
  }
}
