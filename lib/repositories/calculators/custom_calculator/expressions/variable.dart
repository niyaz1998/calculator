import 'primary.dart';

/// represents a variable as an expression
class Variable extends Primary {
  @override
  double calculate(double variableValue) {
    return variableValue;
  }
}
