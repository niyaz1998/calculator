/// [Expression] is a root class.
/// All other tokens represents Expression class and inherited from it.
abstract class Expression {
  /// calculates expression that stored in this object.
  double calculate(double variableValue);
}
