import 'expression.dart';

/// class for representing expression with simple structure (only one child node)
abstract class Primary extends Expression {}
