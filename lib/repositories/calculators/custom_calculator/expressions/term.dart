import 'expression.dart';

/// represents addition and subtraction operators as expression
class Term extends Expression {
  TermOperator op;
  Expression left, right;

  Term(this.op, this.left, this.right);

  @override
  double calculate(double variableValue) {
    switch (op) {
      case TermOperator.PLUS:
        return left.calculate(variableValue) + right.calculate(variableValue);
      case TermOperator.MINUS:
        return left.calculate(variableValue) - right.calculate(variableValue);
      case TermOperator.NONE:
        return left.calculate(variableValue);
    }
    return null;
  }
}

enum TermOperator { PLUS, MINUS, NONE }
