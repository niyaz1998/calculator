import 'expression.dart';
import 'dart:math';

/// represents power of expression
class PowerOf extends Expression {
  PowerOfOperators op;
  Expression left, right;

  PowerOf(this.op, this.left, this.right);

  @override
  double calculate(double variableValue) {
    switch (op) {
      case PowerOfOperators.POWER:
        return pow(
            left.calculate(variableValue), right.calculate(variableValue));
      case PowerOfOperators.NONE:
        return left.calculate(variableValue);
    }
    return null;
  }
}

enum PowerOfOperators { POWER, NONE }
