import 'expression.dart';
import 'primary.dart';

/// represents expression in parentheses
class Parenthesized extends Primary {
  Expression expression;

  Parenthesized(this.expression);

  @override
  double calculate(double variableValue) {
    return expression.calculate(variableValue);
  }
}
