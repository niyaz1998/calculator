import 'expression.dart';

/// represents factor (dividing and multiplying operators) expression
class Factor extends Expression {
  FactorOperator op;
  Expression left, right;

  Factor(this.op, this.left, this.right);

  @override
  double calculate(double variableValue) {
    switch (op) {
      case FactorOperator.DIV:
        return left.calculate(variableValue) / right.calculate(variableValue);
      case FactorOperator.MULT:
        return left.calculate(variableValue) * right.calculate(variableValue);
      case FactorOperator.NONE:
        return left.calculate(variableValue);
    }
    return null;
  }
}

enum FactorOperator { DIV, MULT, NONE }
