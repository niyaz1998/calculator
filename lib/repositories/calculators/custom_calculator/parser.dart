import 'expressions/expression.dart';
import 'expressions/sqrt.dart';
import 'expressions/term.dart';
import 'expressions/factor.dart';
import 'expressions/number.dart';
import 'expressions/parenthesized.dart';
import 'expressions/variable.dart';
import 'expressions/power_of.dart';

/// parses expressions and returns tree of [Expressions]'s
class Parser {
  String _input;

  String get input => _input;

  Parser(String expression) {
    _input = expression.replaceAll(' ', '');

    /// for supporting unary minus
    _input = _input.replaceAll('(-', '(0-');
    if (_input.length > 2 &&
        _input[0] == '-' &&
        int.tryParse(_input[1]) != null) {
      _input = '0' + _input;
    }
  }

  Expression parse() {
    try {
      return _parseTerm();
    } on RangeError catch (e, s) {
      if (e.toString() ==
          'RangeError (index): Invalid value: Valid value range is empty: 0') {
        throw 'something wrong at the end of expression';
      } else {
        rethrow;
      }
    }
  }

  Expression _parseTerm() {
    Expression result = _parseFactor();
    while (_input.isNotEmpty) {
      TermOperator op = _parseTermOperator();
      if (op != TermOperator.NONE) {
        Expression right = _parseFactor();
        result = Term(op, result, right);
      } else {
        break;
      }
    }
    return result;
  }

  TermOperator _parseTermOperator() {
    if (_input[0] == '+') {
      _input = _input.substring(1);
      return TermOperator.PLUS;
    } else if (_input[0] == '-') {
      _input = _input.substring(1);
      return TermOperator.MINUS;
    }
    return TermOperator.NONE;
  }

  Expression _parseFactor() {
    Expression result = _parsePowerOf();
    while (_input.isNotEmpty) {
      FactorOperator op = _parseFactorOperator();
      if (op != FactorOperator.NONE) {
        Expression right = _parsePowerOf();
        result = new Factor(op, result, right);
      } else {
        break;
      }
    }
    return result;
  }

  FactorOperator _parseFactorOperator() {
    if (_input[0] == '*') {
      _input = _input.substring(1);
      return FactorOperator.MULT;
    } else if (_input[0] == '/') {
      _input = _input.substring(1);
      return FactorOperator.DIV;
    }
    return FactorOperator.NONE;
  }

  Expression _parsePowerOf() {
    Expression result = _parsePrimary();
    while (_input.isNotEmpty) {
      PowerOfOperators op = _parsePowerOfOperator();
      if (op != PowerOfOperators.NONE) {
        Expression right = _parsePrimary();
        result = new PowerOf(op, result, right);
      } else {
        break;
      }
    }
    return result;
  }

  PowerOfOperators _parsePowerOfOperator() {
    if (_input[0] == '^') {
      _input = _input.substring(1);
      return PowerOfOperators.POWER;
    }
    return PowerOfOperators.NONE;
  }

  Expression _parsePrimary() {
    Expression result;
    if (int.tryParse(_input[0]) != null) {
      result = _parseNumber();
    } else if (_input[0] == '(') {
      _input = _input.substring(1);
      result = Parenthesized(_parseTerm());
      _input = _input.substring(1); // skip ‘)’
    } else if (_input[0] == 'x') {
      _input = _input.substring(1);
      result = Variable();
    } else if (_input.length >= 7 && _input.substring(0, 5) == 'sqrt(') {
      _input = _input.substring(5);
      result = SqrtExpression(_parseTerm());
      _input = _input.substring(1); // skip ‘)’
    } else {
      throw 'Unknown character ${_input[0]}';
    }
    return result;
  }

  Expression _parseNumber() {
    String number = '';
    int i = 0;
    while (i < _input.length && int.tryParse(_input[i]) != null) {
      number = number + _input[i];
      i++;
    }
    if (i < _input.length && _input[i] == '.') {
      number = number + _input[i];
      i++;
      while (i < _input.length && int.tryParse(_input[i]) != null) {
        number = number + _input[i];
        i++;
      }
    }
    _input = _input.substring(number.length);
    return NumberExpression(double.parse(number));
  }
}
