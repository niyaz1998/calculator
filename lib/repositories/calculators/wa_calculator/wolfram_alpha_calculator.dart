import 'dart:convert';
import 'dart:math';

import 'package:expression_caculator/helpers/api_helper.dart';
import 'package:expression_caculator/model/wolfram_alpha/query_result.dart';

import '../calculator.dart';

/// this repository is using wolfram alpha to return result
///
/// it makes an api call to wolfram alpha https://products.wolframalpha.com/api/documentation/
/// then it parses a result of call and returns it
///
/// WARNING: parsing of wolfram alpha can easily fail, according to the fact what parsing output is made using some assumptions
/// in that case this repository will throw an error 'error during parsing wolfram alpha response, response: ${wolframAlphaResponse}';
class WolframAlphaCalculatorRepository extends AbstractCalculatorRepository {
  @override
  String get name => 'Wolfram Alpha';

  @override
  Future<List<Point<double>>> getChart(
    String expression,
    double startPoint,
    double endPoint, [
    int pointsAmount = 100,
  ]) async {
    // swap values if starting point is after last point
    if (startPoint > endPoint) {
      double temp = endPoint;
      endPoint = startPoint;
      startPoint = temp;
    }

    double stepSize = (endPoint - startPoint) / pointsAmount;

    QueryResult response =
        await ApiHelper.getPoints(expression, startPoint, endPoint, stepSize);

    try {
      /// find resulting pod and get plaintext of his first subpod
      /// ASSUMPTION: resulting plaintext will be stored in first subpod of pod with id 'Result'
      String plaintTextResult = response.pods
          .where((Pod pod) => pod.id == 'Result')
          .first
          .subpods
          .first
          .plaintext;

      /// ASSUMPTION: plaintext will be in form of list of numbers separated by ', '
      /// and at the start and end of plaintext there will be '{' and '}' characters
      plaintTextResult = plaintTextResult.replaceAll(' ', '');
      plaintTextResult = plaintTextResult.replaceAll('{', '');
      plaintTextResult = plaintTextResult.replaceAll('}', '');

      double x = startPoint;
      List<Point<double>> result = List();
      for (var value in plaintTextResult.split(',')) {
        if (double.tryParse(value) != null) {
          result.add(Point(x, double.parse(value)));
          x += stepSize;
        }
      }
      if (result.isNotEmpty) {
        return result;
      } else {
        throw 'wolfram alpha did not return a valid result';
      }
    } catch (e, s) {
      print(e);
      print(s);
      throw 'error during parsing wolfram alpha response, response: ${json.encode(response.toJson())}';
    }
  }
}
