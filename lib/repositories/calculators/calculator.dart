import 'dart:math';

import 'custom_calculator/custom_calculator.dart';
import 'wa_calculator/wolfram_alpha_calculator.dart';

/// represents abstract calculator repository which can be used to calculate a chart
abstract class AbstractCalculatorRepository {
  /// returns list of points for drawing a chart for a given [expression]
  Future<List<Point<double>>> getChart(
    String expression,
    double startPoint,
    double endPoint, [
    int pointsAmount = 100,
  ]);

  /// returns a name for this calculator
  String get name;
}

/// all calculators which are currently used in application
List<AbstractCalculatorRepository> allCalculators = [
  CustomCalculatorRepository(),
  WolframAlphaCalculatorRepository(),
];
