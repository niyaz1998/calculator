import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:expression_caculator/repositories/calculators/calculator.dart';

import 'event.dart';
import 'state.dart';

/// BLoC which handles state of screen with calculators
class CalculatorsBloc extends Bloc<CalculatorEvent, CalculatorState> {
  @override
  get initialState => CalculatorState(
      calculationInProgress: false,
      calculator: allCalculators.isNotEmpty ? allCalculators[0] : null);

  @override
  Stream<CalculatorState> mapEventToState(CalculatorEvent event) async* {
    if (event is CalculatorSelected) {
      yield CalculatorState(
          calculator: event.calculator,
          points: currentState.points,
          calculationInProgress: currentState.calculationInProgress,
          error: currentState.error);
    } else if (event is CalculationStarted) {
      yield CalculatorState(
          calculator: currentState.calculator, calculationInProgress: true);
    } else if (event is CalculationFinished) {
      yield CalculatorState(
          calculator: currentState.calculator,
          points: event.points,
          calculationInProgress: false);
    } else if (event is ErrorOccurred) {
      yield CalculatorState(
          calculator: currentState.calculator,
          error: event.error,
          calculationInProgress: false);
    }
  }

  /// allows to change calculator
  onCalculatorSelect(AbstractCalculatorRepository calculator) {
    dispatch(CalculatorSelected(calculator));
  }

  /// starts calculation of an expression
  ///
  /// in case of success will dispatch state with list of points
  /// in case of error will dispatch state with error
  onCalculateClick(String expression, double startPoint, double endPoint) {
    if (currentState.calculator == null) {
      dispatch(ErrorOccurred('Choose calculator'));
      return;
    }
    dispatch(CalculationStarted());
    currentState.calculator
        .getChart(expression, startPoint, endPoint)
        .then((List<Point<double>> points) {
      dispatch(CalculationFinished(points));
    }).catchError((e, s) {
      print(s);
      dispatch(ErrorOccurred(e));
    });
  }
}
