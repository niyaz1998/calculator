import 'dart:math';

import 'package:expression_caculator/repositories/calculators/calculator.dart';

class CalculatorState {
  dynamic error;

  List<Point<double>> points;

  AbstractCalculatorRepository calculator;

  bool calculationInProgress;

  CalculatorState({
    this.error,
    this.points,
    this.calculator,
    this.calculationInProgress,
  });
}
