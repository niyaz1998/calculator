import 'dart:math';

import 'package:expression_caculator/repositories/calculators/calculator.dart';

abstract class CalculatorEvent {}

class CalculationStarted extends CalculatorEvent {}

class ErrorOccurred extends CalculatorEvent {
  dynamic error;

  ErrorOccurred(this.error);
}

class CalculationFinished extends CalculatorEvent {
  List<Point<double>> points;

  CalculationFinished(this.points);
}

class CalculatorSelected extends CalculatorEvent {
  AbstractCalculatorRepository calculator;

  CalculatorSelected(this.calculator);
}
