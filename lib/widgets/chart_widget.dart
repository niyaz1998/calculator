import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:math';

/// draws a chart for a given points
///
/// doesn't draw X or Y axises
/// draws only given points and two lines parallel to X and Y axises
/// marks start and end of lines
class ChartWidget extends StatelessWidget {
  final List<Point<double>> points;

  ChartWidget(this.points);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: CustomPaint(
          painter: _ChartPainter(points),
          child: Container(),
        ));
  }
}

class _ChartPainter extends CustomPainter {
  final List<Point<double>> points;

  /// [TEXT_SIZE] - is font size of text used as labels
  ///
  /// [TEXT_SIZE] * 2 - is offset of chart for Y axis, for text under the chart
  static const double TEXT_SIZE = 8;

  /// [MAX_TEXT_WIDTH] - is maximum width of text labels on the left side of the chart
  ///
  /// [MAX_TEXT_WIDTH] - is offset of chart for X axis, for text on left side of chart
  static const double MAX_TEXT_WIDTH = 16;

  _ChartPainter(this.points);

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    /// boundaries of chart
    double maxX = double.negativeInfinity;
    double maxY = double.negativeInfinity;
    double minX = double.infinity;
    double minY = double.infinity;

    points.forEach((Point<double> point) {
      maxX = max(point.x, maxX);
      maxY = max(point.y, maxY);
      minX = min(point.x, minX);
      minY = min(point.y, minY);
    });

    drawTexts(canvas, size, maxX, maxY, minX, minY);
    drawChart(canvas, size, maxX, maxY, minX, minY);
  }

  void drawTexts(Canvas canvas, Size size, double maxX, double maxY,
      double minX, double minY) {
    TextStyle textStyle = TextStyle(color: Colors.black, fontSize: TEXT_SIZE);

    TextPainter startX = TextPainter(
      text: TextSpan(
        text: prettyDouble(minX),
        style: textStyle,
      ),
      textDirection: TextDirection.ltr,
    );

    TextPainter endX = TextPainter(
      text: TextSpan(
        text: prettyDouble(maxX),
        style: textStyle,
      ),
      textDirection: TextDirection.rtl,
    );

    TextPainter startY = TextPainter(
      text: TextSpan(
        text: prettyDouble(minY),
        style: textStyle,
      ),
      textDirection: TextDirection.ltr,
    );

    TextPainter endY = TextPainter(
      text: TextSpan(
        text: prettyDouble(maxY),
        style: textStyle,
      ),
      textDirection: TextDirection.ltr,
    );

    startX.layout(maxWidth: (size.width - MAX_TEXT_WIDTH) / 2);
    endX.layout(maxWidth: (size.width - MAX_TEXT_WIDTH) / 2);
    startY.layout(maxWidth: MAX_TEXT_WIDTH);
    endY.layout(maxWidth: MAX_TEXT_WIDTH);

    startX.paint(canvas, Offset(MAX_TEXT_WIDTH, size.height - TEXT_SIZE));
    endX.paint(
        canvas, Offset(size.width - endX.width, size.height - TEXT_SIZE));

    /// TEXT_SIZE here is multiplied by 3 because,
    /// TEXT_SIZE multiplied by 2 is offset of the chart along Y axis
    /// and also text paints from the top so we need to give another TEXT_SIZE space
    startY.paint(canvas, Offset(0, size.height - TEXT_SIZE * 3));
    endY.paint(canvas, Offset(0, 0));
  }

  void drawChart(Canvas canvas, Size size, double maxX, double maxY,
      double minX, double minY) {
    double chartWidth = size.width - MAX_TEXT_WIDTH;
    double chartHeight = size.height - TEXT_SIZE * 2;

    Function getXOffset =
        (double x) => (x - minX) / (maxX - minX) * chartWidth + MAX_TEXT_WIDTH;

    Function getYOffset = (double y) {
      double newY = ((y - minY) / (maxY - minY) * chartHeight) + TEXT_SIZE * 2;
      return size.height - newY; // because 0 of y starts from top
    };

    Paint paint = Paint();
    paint.color = Colors.red;
    paint.strokeWidth = 2;

    canvas.drawPoints(
        PointMode.points,
        points
            .map((Point<double> point) =>
                Offset(getXOffset(point.x), getYOffset(point.y)))
            .toList(),
        paint);

    paint.color = Colors.black;
    canvas.drawPoints(
        PointMode.polygon,
        [
          Offset(MAX_TEXT_WIDTH, 0),
          Offset(MAX_TEXT_WIDTH, chartHeight),
          Offset(size.width, chartHeight),
        ],
        paint);
  }

  /// formats double, returns maximum two numbers after the point if number is not whole number
  String prettyDouble(double d) =>
      d.toStringAsFixed(d.truncateToDouble() == d ? 0 : 2);
}
