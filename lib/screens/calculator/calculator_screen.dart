import 'package:expression_caculator/bloc/calculator/bloc.dart';
import 'package:expression_caculator/repositories/calculators/calculator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'chart_calculator_part.dart';

/// screen for drawing a charts
///
/// composed of two main parts
/// - for entering user input. Such as expression, x0, xN and selecting a calculator to apply on expression
/// - for drawing a result to user. Such as chart itself, error message or wait widget
class CalculatorScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  CalculatorsBloc bloc;
  final _formKey = GlobalKey<FormState>();
  TextEditingController expressionController, startController, endController;

  static const ALLOWED_EXPRESSION_CHARS = '0123456789()+-/*.^x sqrt';

  @override
  void initState() {
    bloc = CalculatorsBloc();
    _initControllers();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
        appBar: AppBar(title: Text('Calculators')),
        body: BlocProvider(
            bloc: bloc,
            child: BlocBuilder(
                builder: (context, state) => Column(
                      children: <Widget>[
                        _content,
                        Expanded(child: ChartCalculatorPart())
                      ],
                    ),
                bloc: bloc)));
  }

  Widget get _content => Container(
        padding: EdgeInsets.all(16),
        child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  controller: expressionController,
                  decoration: InputDecoration(labelText: 'expression'),
                  validator: _checkExpressionTextField,
                ),
                SizedBox(height: 8),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: startController,
                      decoration: InputDecoration(labelText: 'starting point'),
                      validator: _checkFloatTextField,
                    )),
                    SizedBox(width: 16),
                    Expanded(
                        child: TextFormField(
                            keyboardType: TextInputType.number,
                            controller: endController,
                            decoration: InputDecoration(labelText: 'end point'),
                            validator: _checkFloatTextField))
                  ],
                ),
                SizedBox(height: 8),
                _calculatorSelector,
                SizedBox(height: 8),
                _submitButton,
                SizedBox(height: 8),
              ],
            )),
      );

  Widget get _calculatorSelector {
    List<DropdownMenuItem<AbstractCalculatorRepository>> calculatorsMenuItems =
        allCalculators
            .map((AbstractCalculatorRepository calc) =>
                DropdownMenuItem<AbstractCalculatorRepository>(
                  child: Text(calc.name),
                  value: calc,
                ))
            .toList();

    return DropdownButton<AbstractCalculatorRepository>(
        items: calculatorsMenuItems,
        isExpanded: true,
        value: bloc.currentState.calculator,
        onChanged: (AbstractCalculatorRepository calc) {
          bloc.onCalculatorSelect(calc);
        });
  }

  Widget get _submitButton => RaisedButton(
      child: Text('Calculate'),
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      onPressed: () {
        if (_formKey.currentState.validate()) {
          bloc.onCalculateClick(
              expressionController.text,
              double.tryParse(startController.text),
              double.tryParse(endController.text));
        }
      });

  String _checkFloatTextField(String text) =>
      double.tryParse(text) == null ? 'Enter float number' : null;

  String _checkExpressionTextField(String text) {
    for (var value in text.split('')) {
      if (!ALLOWED_EXPRESSION_CHARS.contains(value)) {
        return 'unallowed charatcer $value';
      }
    }
    return null;
  }

  void _initControllers() {
    endController = TextEditingController(text: '10');
    startController = TextEditingController(text: '0');
    expressionController =
        TextEditingController(text: 'x^2 + sqrt( - 1 + 2 / 2 + 4 * (8 - 4))');
  }
}
