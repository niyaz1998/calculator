import 'package:expression_caculator/bloc/calculator/bloc.dart';
import 'package:expression_caculator/widgets/chart_widget.dart';
import 'package:expression_caculator/widgets/wait_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// displays result of calculator
///
/// have 4 possible outputs
///  - empty container, which means user haven't done anything yet
///  - wait widget - calculation is in progress
///  - error message - some error occurred during computation
///  - chart - a chart
class ChartCalculatorPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CalculatorsBloc bloc = BlocProvider.of<CalculatorsBloc>(context);
    if (bloc.currentState.calculationInProgress) {
      return WaitWidget();
    } else if (bloc.currentState.error != null) {
      return SingleChildScrollView(
          child: Container(
        padding: EdgeInsets.all(16),
        child: Text("Error occurred: \n${bloc.currentState.error}"),
      ));
    } else if (bloc.currentState.points != null) {
      return Container(
          padding: EdgeInsets.all(16),
          child: ChartWidget(bloc.currentState.points));
    }
    return Container();
  }
}
